/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  Test Offline
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeURL.h"
#import <Cordova/CDVViewController.h>

@implementation HomeURL
- (void)load:(CDVInvokedUrlCommand*)command  {
    
    NSLog(@"Executing action 'load' on IOS 'homeurl' plugin");
    
    // Obtain the main Cordova view controller that hosts the webview.
    
    UIViewController* viewController = self.viewController;
    CDVViewController* cdvViewController = (CDVViewController*) viewController;
    
    // Obtain start page from view controller and build new request with it.
    
    NSString* startPage = cdvViewController.startPage;
    NSURL* url = [NSURL URLWithString:startPage];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSString* outputMessage;
    CDVPluginResult* pluginResult = nil;
    
    @try {
        
        [self.webViewEngine loadRequest:request];
        
    }
    
    @catch (NSException *exception) {
        
        NSLog(@"URL load not successful");
        outputMessage = [exception reason];
        NSLog(@"%@", outputMessage);
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:outputMessage];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                        
        @throw;
    }
                        
    outputMessage = @"URL load initiated successfully";
    NSLog(@"%@", outputMessage);
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:outputMessage];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
                        
                        
