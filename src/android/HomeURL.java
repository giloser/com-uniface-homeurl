package com.uniface.homeurl;

import org.apache.cordova.*;
import android.app.Activity;
import org.json.JSONException;


// This plugin allows the app to load the Home URL from Javascript
public class HomeURL extends CordovaPlugin {

    private static final String TAG = "HomeURL";
    private CordovaActivity activity;
    private String launchUrl;
    
    @Override
    public boolean execute(final String action, final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {

        String outputMessage;

        final CordovaActivity refActivity = this.activity;
        final String refLaunchUrl = this.launchUrl;

        if ("load".equals(action)) {

            LOG.v(TAG, "Executing action 'load' on android 'homeurl' plugin");

            // The execute method runs on the WebCore thread (not the UI thread)
            // (The same thread that Javascript executes on).
            // Any updates to the UI (such as loading a new URL)
            // must be explicitly run on the UI thread.

            this.activity.runOnUiThread(new Runnable() {

              @Override
              public void run() {
                  
                  // loadUrl does not throw exceptions if it encounters
                  // problems loading pages (see Android WebView documentation)
                  // So no exception handler here.
                  refActivity.loadUrl(refLaunchUrl);

                  String outputMessage = String.format("URL load initiated successfully");
                  LOG.v(TAG, outputMessage);

                  callbackContext.success(outputMessage);
              }
            });

            return true;
        }

        outputMessage = String.format("Could not find action '%s' on android 'homeurl' plugin", action);
        LOG.v(TAG, outputMessage);
        callbackContext.error(outputMessage);

        // Return false to trigger 'MethodNotFound' mechanism.
        return false;
    }

    @Override
    public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
        LOG.v(TAG, "initialization");
        super.initialize(cordova, webView);

        this.activity = (CordovaActivity)this.cordova.getActivity();

        // The Launch URL is protected in CordovaActivity so
        // it is read from the configuration anew.

        ConfigXmlParser parser = new ConfigXmlParser();
        parser.parse(this.activity.getApplicationContext());
        this.launchUrl = parser.getLaunchUrl();
    }
}
